# A research paper a week

## List of papers

* [A Neural Representation of Sketch Drawings](https://arxiv.org/pdf/1704.03477.pdf)
* [Video Summarization using Deep Semantic Features](https://arxiv.org/pdf/1609.08758.pdf)
* [Deep Learning for Hate Speech Detection in Tweets](http://papers.www2017.com.au.s3-website-ap-southeast-2.amazonaws.com/companion/p759.pdf)
* [Unsupervised Video Summarization with Adversarial LSTM Networks](http://web.engr.oregonstate.edu/~sinisa/research/publications/cvpr17_summarization.pdf)
* [Generative Adversarial Text to Image Synthesis](https://arxiv.org/pdf/1605.05396.pdf)
* [Deep Visual-Semantic Alignments for Generating Image Descriptions](http://cs.stanford.edu/people/karpathy/cvpr2015.pdf)
* [YouTube-8M: A Large-Scale Video Classification Benchmark](https://arxiv.org/pdf/1609.08675.pdf)
* [CNN ARCHITECTURES FOR LARGE-SCALE AUDIO CLASSIFICATION](https://arxiv.org/pdf/1609.09430.pdf)
* [CO-REGULARIZED DEEP REPRESENTATIONS FOR VIDEO SUMMARIZATION](http://vijaychan.github.io/Publications/2015%20ICIP-%20Correguarlized%20Deep%20Representations%20for%20Video%20Summarization.pdf)
* [ReNet: A Recurrent Neural Network Based Alternative to Convolutional Networks](https://arxiv.org/pdf/1505.00393.pdf)
* [Re3 : Real-Time Recurrent Regression Networks for Object Tracking](https://arxiv.org/pdf/1705.06368.pdf)
* [Summarization-based Video Caption via Deep Neural Networks](file:///home/saumo/Downloads/p1191-li.pdf)
* [Translating Videos to Natural Language Using Deep Recurrent Neural Networks](https://arxiv.org/pdf/1412.4729.pdf)